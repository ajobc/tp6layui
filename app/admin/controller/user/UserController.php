<?php

namespace app\admin\controller;

use app\common\controller\BaseController;

class UserController extends BaseController
{
    public function index()
    {
        return "public";
    }

    public function hello($name = 'ThinkPHP6')
    {
        return 'hello,' . $name;
    }
}
