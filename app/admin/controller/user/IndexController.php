<?php

namespace app\admin\controller\user;

use app\admin\controller\AuthController;

class IndexController extends AuthController
{
    public function index()
    {
        dump($this->moduleName);
        dump($this->controllerName);
        dump($this->actionName);
        dump("root_path:" . root_path('public'));
        dump("public_path:" . public_path());
        dump(runtime_path());
        dump(config_path());
        dump(base_path());
        $apppath = (app_path());
        dump("public_path:" . public_path('public'));
        return display("", ['apppath' => $apppath]);
    }

    public function hello($name = 'ThinkPHP6')
    {
        return 'hello,' . $name;
    }
}
