<?php

namespace app\admin\controller\cases;

use app\admin\controller\AuthController;
use app\admin\model\cases\CasetalkModel;

/**
 * Class IndexController
 * @package app\admin\controller
 */
class CasetalkController extends AuthController
{
    public function index()
    {
        if (!$this->isPost) {
            $case_id = input('case_id');
            $this->assign('case_id', $case_id);
            return $this->fetch();
        } else {
            $model = new CasetalkModel();
            $list = $model->getIndexData();
            return json(['rows' => $list->toArray()['data'], 'total' => $list->total()]);
            // return $list->toArray()['data'];
        }
    }

    public function add()
    {
    }

    public function edit()
    {
        if (!$this->isPost) {
            // $this->assign('jingjiaqudaolist', SysjingjiaqudaoService::getAllList());
            return $this->fetch();
        } else {
            $params = input('post.');
            $model = CasetalkModel::yqUpdate($params);
            $this->success("成功！", "", $model);
        }
    }

    public function delete()
    {
        $ids = input('ids');
        $res = CasetalkModel::yqDeleteByIds($ids);
        $this->success("成功！", "", $res);
    }
}
