<?php

namespace app\admin\controller\cases;

use app\admin\controller\AuthController;
use app\admin\model\cases\CasecontactModel;
use app\admin\model\cases\CasetalkModel;
use app\admin\validate\cases\CasecontactValidate;

/**
 * Class IndexController
 * @package app\admin\controller
 */
class CasecontactController extends AuthController
{
    public function index()
    {
        if (!$this->isPost) {
            $case_id = input('case_id');
            if (empty($case_id)) {
                return ('请从案件管理列表打开联系人管理！');
            }
            $this->assign('case_id', $case_id);
            return $this->fetch();
        } else {
            $model = new CasecontactModel();
            $list = $model->getIndexData();
            return json(['rows' => $list->toArray()['data'], 'total' => $list->total()]);
            // return $list->toArray()['data'];
        }
    }

    public function add()
    {
        if ($this->isPost) {
            $params = input('post.');
            unset($params['id']);
            $validate = new CasecontactValidate();
            if (!$validate->check($params)) {
                $this->error($validate->getError());
                exit;
            }
            try {
                $res = CasecontactModel::yqCreate($params);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success("成功！", "", $params);
        } else {
            return $this->fetch();
        }
    }

    public function edit()
    {
        if (!$this->isPost) {
            $case_id = input('case_id');
            $this->assign('case_id', $case_id);
            return $this->fetch();
        } else {
            $params = input('post.');
            $validate = new CasecontactValidate();
            if (!$validate->check($params)) {
                $this->error($validate->getError());
                exit;
            }
            $model = CasecontactModel::yqUpdate($params);
            $this->success("成功！", "", $model);
        }
    }

    public function delete()
    {
        $ids = input('ids');
        $res = CasecontactModel::yqDeleteByIds($ids);
        $this->success("成功！", "", $res);
    }
}
