<?php

namespace app\admin\controller\system;

use app\admin\controller\AuthController;
use app\admin\controller\WidgetController;
use app\admin\model\system\SyscasetypeModel;
use app\admin\model\system\SysdepartmentModel;
use app\admin\validate\system\SyscasetypeValidate;
use app\admin\validate\system\SysmenuValidate;
use think\Db;

class SyscasetypeController extends AuthController
{
    public function index()
    {
        if (!$this->isPost) {
            return $this->fetch();
        } else {
            $model = new SyscasetypeModel();
            $list = $model->getIndexData();
            return json(['rows' => $list->toArray()['data'], 'total' => $list->total()]);
            // return $list->toArray()['data'];
        }
    }

    public function add()
    {
        if ($this->isPost) {
            $params = input('post.');
            unset($params['id']);
            $validate = new SyscasetypeValidate();
            if (!$validate->check($params)) {
                $this->error($validate->getError());
                exit;
            }
            try {
                $res = SyscasetypeModel::yqCreate($params);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success("成功！", "", $params);
        } else {
            return $this->fetch();
        }
    }

    public function edit()
    {
        if ($this->isPost) {
            $params = input('post.');
            SyscasetypeModel::yqUpdate($params);
            $this->success("成功！", "", $params);
        } else {
            return $this->fetch();
        }
    }

    public function delete()
    {
        $ids = input('ids');
        $res = SyscasetypeModel::yqDeleteByIds($ids);
        $this->success("成功！", "", $res);
    }
}
