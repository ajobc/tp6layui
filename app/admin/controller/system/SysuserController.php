<?php

namespace app\admin\controller\system;

use app\admin\controller\AuthController;
use app\admin\model\system\SysroleModel;
use app\admin\model\system\SysuserModel;
use app\admin\validate\system\SysroleValidate;
use app\admin\validate\system\SysuserValidate;
use app\common\service\system\SysdepartmentService;
use app\common\service\system\SysmenuService;
use app\common\service\system\SysprojectService;
use app\common\service\system\SysroleService;
use think\Exception;
use think\facade\Cache;

class SysuserController extends AuthController
{
    public function index()
    {
        if (!$this->isPost) {
            $this->assign('departmentlist', SysdepartmentService::getAllList());
            $this->assign('projectlist', SysprojectService::getAllList());
            return $this->fetch();
        } else {
            $sysuserModel = new SysuserModel();
            $sysuserlist = $sysuserModel->getIndexData();
            $this->success("成功！", "", $sysuserlist);
        }
    }

    public function add()
    {
        if ($this->isPost) {
            $sysuserModel = new SysuserModel();
            $params = input('post.');
            $params['password'] = md5(123456);
            $params['bind_p'] = '123456';
            $params['bind_account'] = $params['account'];
            $roleids = input('post.role_ids');
            $project_ids = input('post.project_ids');
            if (empty($roleids)) {
                $this->error("请选择角色！", "", $params);
                exit;
            }
            if (empty($project_ids)) {
                $this->error("请选择机构！", "", $params);
                exit;
            }
            $validate = new SysuserValidate();
            if (!$validate->check($params)) {
                $this->error($validate->getError());
                exit;
            }
            $insertid = $sysuserModel->insertGetId($params);
            $sysuser = SysuserModel::find($insertid);
            $sysuser->roles()->detach();//删除关联中间表数据
            $sysuser->roles()->saveAll($roleids);
            $sysuser->projects()->detach();//删除关联中间表数据
            $sysuser->projects()->saveAll($project_ids);
            $this->success("成功！", "", $params);
        } else {
            return $this->fetch();
        }
    }

    public function edit()
    {
        if ($this->isPost) {
            $params = input('post.');
            $roleIds = input('post.roleIds');
            $project_ids = input('post.project_ids');
            SysuserModel::yqUpdate($params);
            $sysrole = SysuserModel::find($params['id']);
            $sysrole->roles()->detach();//删除关联中间表数据
            $sysrole->roles()->saveAll($roleIds);
            $sysrole->projects()->detach();//删除关联中间表数据
            //$sysrole->projects()->saveAll($project_ids);
            $this->success("成功！", "", $params);
        } else {
            $this->assign('rolelist', SysroleService::getAllList());
            $this->assign('departmentlist', SysdepartmentService::getAllList());
            $this->assign('projectlist', SysprojectService::getAllList());
            return $this->fetch();
        }
    }

    // 更新数据
    public function editpassword()
    {
        if ($this->isPost) {
            $params = input('post.');
            $data['id'] = $params['id'];
            $data['bind_p'] = ($params['password']);
            $data['password'] = md5($params['password']);
            try {
                SysuserModel::yqUpdate($data);
            } catch (Exception $exception) {
                $this->error($exception->getMessage());
            }
            $this->success('更新成功！', 1);
        } else {
            return $this->fetch();
        }
    }

    public function delete()
    {
        $id = input('param.id');
        $sysmodel = new SysuserModel();
        $res = $sysmodel::destroy($id);
        $this->success("成功！", "", $res);
    }

    public function clearCache()
    {
        $info = Cache::clear();
        $msg = ($info == true) ? "清除缓存成功！" : "清除缓存失败！";
        $this->success($msg);
    }
}