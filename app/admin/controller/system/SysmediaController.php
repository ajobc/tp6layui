<?php

namespace app\admin\controller\system;

use app\admin\controller\AuthController;
use app\admin\controller\WidgetController;
use app\admin\model\system\SysmediaModel;
use app\admin\model\system\SysdepartmentModel;
use app\admin\validate\system\SyscasetypeValidate;
use app\admin\validate\system\SysmediatypeValidate;
use app\admin\validate\system\SysmediaValidate;
use app\admin\validate\system\SysmenuValidate;
use think\Db;

class SysmediaController extends AuthController
{
    public function index()
    {
        if (!$this->isPost) {
            return $this->fetch();
        } else {
            $model = new SysmediaModel();
            $list = $model->getIndexData();
            return json(['rows' => $list->toArray()['data'], 'total' => $list->total()]);
            // return $list->toArray()['data'];
        }
    }

    public function add()
    {
        if ($this->isPost) {
            $params = input('post.');
            unset($params['id']);
            $validate = new SysmediaValidate();
            if (!$validate->check($params)) {
                $this->error($validate->getError());
                exit;
            }
            try {
                $res = SysmediaModel::yqCreate($params);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success("成功！", "", $params);
        } else {
            return $this->fetch();
        }
    }

    public function edit()
    {
        if ($this->isPost) {
            $params = input('post.');
            SysmediaModel::yqUpdate($params);
            $this->success("成功！", "", $params);
        } else {
            return $this->fetch();
        }
    }

    public function delete()
    {
        $ids = input('ids');
        $res = SysmediaModel::yqDeleteByIds($ids);
        $this->success("成功！", "", $res);
    }
}
