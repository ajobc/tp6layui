<?php

namespace app\admin\controller\system;

use app\admin\controller\AuthController;
use app\admin\controller\WidgetController;
use app\admin\model\system\SysmenuModel;
use app\admin\validate\system\SysmenuValidate;
use app\common\service\system\SysmenuService;
use think\Db;

class SysmenuController extends AuthController
{
    public function index()
    {
        //$sysmenu=SysuserModel::all();
        //dump($sysmenu);
        if (!$this->isPost) {
            return $this->fetch();
        } else {
            $sysmenu = new SysmenuModel();
            $sysmenulist = $sysmenu->yqGetAllData(null, '*', 'id asc');
            // $sysmenulist = $sysmenu->select();
            $json = [
                "rows" => $sysmenulist,
            ];
            $this->success("成功！", "", $sysmenulist);
        }
    }

    public function add()
    {
        if ($this->isPost) {
            $params = input('post.');
            $validate = new SysmenuValidate();
            if (!$validate->check($params)) {
                $this->error($validate->getError());
                exit;
            }
            try {
                $res = SysmenuModel::create($params);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success("成功！", "", $params);
        } else {
            $sysmenuModel = new SysmenuModel();
            $sysmenulist = $sysmenuModel->select();
            $this->assign("nodelist", $sysmenulist);
            return $this->fetch();
        }
    }

    public function edit()
    {
        if ($this->isPost) {
            $params = input('post.');
            SysmenuModel::update($params);
            $this->success("成功！", "", $params);
        } else {
            $menuid = input("id");
            $sysmenuModel = new SysmenuModel();
            $sysmenu = $sysmenuModel->yqGetOne(['id' => $menuid]);
            $this->assign("sysMenu", $sysmenu);
            $this->assign('menulist', SysmenuService::getAllList());
            return $this->fetch();
        }
    }

    public function delete()
    {
        $id = input('id');
        $sysmenu = new SysmenuModel();
        $res = $sysmenu::destroy($id);
        $this->success("成功！", "", $res);
    }
}
