<?php

namespace app\admin\controller\staff;

use app\admin\controller\AuthController;
use app\admin\model\staff\StaffPeixunModel;

/**
 * Class IndexController
 * @package app\admin\controller
 */
class StaffpeixunController extends AuthController
{
    public function index()
    {
        if (!$this->isPost) {
            $staff_id = input('staff_id');
            $this->assign('staff_id', $staff_id);
            return $this->fetch();
        } else {
            $model = new StaffPeixunModel();
            $list = $model->getIndexData();
            return json(['rows' => $list->toArray()['data'], 'total' => $list->total()]);
            // return $list->toArray()['data'];
        }
    }

    public function add()
    {
        if (!$this->isPost) {
            // $this->assign('jingjiaqudaolist', SysjingjiaqudaoService::getAllList());
            return $this->fetch();
        } else {
            $params = input('post.');
            $params['attachment']=json_encode($params['attachment']);
            $model = StaffPeixunModel::yqCreate($params);
            $this->success("成功！", "", $model);
        }
    }

    public function edit()
    {
        if (!$this->isPost) {
            return $this->fetch();
        } else {
            $params = input('post.');
            $params['attachment']=json_encode($params['attachment']);
            $model = StaffPeixunModel::yqUpdate($params);
            $this->success("成功！", "", $model);
        }
    }

    public function delete()
    {
        $ids = input('ids');
        $res = StaffPeixunModel::yqDeleteByIds($ids);
        $this->success("成功！", "", $res);
    }
}
