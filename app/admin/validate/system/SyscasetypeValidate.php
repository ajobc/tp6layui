<?php

namespace app\admin\validate\system;

use think\Validate;

class SyscasetypeValidate extends Validate
{
    protected $rule = [
        'name'  => 'require|max:255|unique:syscasetype',
    ];
    protected $message = [
        'name.require'  => '名称必须！',
        'name.max'      => '名称最多不能超过255个字符！',
        'name.unique'   => '名称已经存在！',
    ];
}