<?php

namespace app\admin\model\staff;

use app\admin\model\system\SysprojectModel;
use app\admin\model\system\SysuserModel;
use app\common\model\CommonModel;

class StaffDealModel extends CommonModel
{
    protected $name = "staff_deal";
    protected $autoWriteTimestamp = 'datetime';

    public function staff()
    {
        return $this->belongsTo(StaffModel::class, 'staff_id');
    }

    public function project()
    {
        return $this->belongsTo(SysprojectModel::class, 'project_id');
    }

    public function user()
    {
        return $this->belongsTo(SysuserModel::class, 'user_id');
    }

    public static function onBeforeInsert($model)
    {
        $model->project_id = session('projectid');
        $model->project_name = session('projectname');
        $model->user_id = session('sysuser.id');
        $model->user_name = session('sysuser.account');
    }

    public function getIndexData()
    {
        $page = input('page');
        $pageSize = input('pageSize');
        $sort = input('sort');
        $order = input('order');
        $where = array();
        $name = input('name');
        $staff_id = input('staff_id');
        if (!empty($name)) {
            $where[] = ['name', 'like', '%' . $name . '%'];
        }
        if (!empty($staff_id)) {
            $where[] = ['staff_id', '=', $staff_id];
        }
        $modellist = $this->where($where)
                          ->with(['staff', 'project', 'user'])
                          ->withAttr('attachment', function ($value, $data) {
                              return json_decode($value);
                          })
                          ->order($sort, $order)->paginate($pageSize);
        // dump($this->getLastSql());
        return $modellist;
    }
}