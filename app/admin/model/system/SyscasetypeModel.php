<?php

namespace app\admin\model\system;

use app\common\model\CommonModel;

class SyscasetypeModel extends CommonModel
{
    protected $name = "syscasetype";
    protected $autoWriteTimestamp = 'datetime';

    public function getIndexData()
    {
        $page = input('page');
        $pageSize = input('pageSize');
        $sort = input('sort');
        $order = input('order');
        $where = array();
        $account = input('account');
        $where[] = ['name', 'like', '%' . $account . '%'];
        $modellist = $this->where($where)->with([])->order("id", "desc")->paginate($pageSize);
        // dump($this->getLastSql());
        return $modellist;
    }
}