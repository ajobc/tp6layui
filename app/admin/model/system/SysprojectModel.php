<?php

namespace app\admin\model\system;

use app\common\model\CommonModel;
use think\facade\Db;

class SysprojectModel extends CommonModel
{
    protected $name = "sysproject";

    public function users()
    {
        return $this->belongsToMany(SysuserModel::class, 'sysproject_user', 'user_id', 'project_id');
    }

    public function mediatypes()
    {
        return $this->belongsToMany(SysmediatypeModel::class, 'sysproject_mediatype', 'mediatype_id', 'project_id');
    }

    public function medias()
    {
        return $this->belongsToMany(SysmediaModel::class, 'sysproject_media', 'media_id', 'project_id');
    }

    public function casetypes()
    {
        return $this->belongsToMany(SyscasetypeModel::class, 'sysproject_casetype', 'casetype_id', 'project_id');
    }

    public function getIndexData()
    {
        $page = input('page');
        $pageSize = input('pageSize');
        $sort = input('sort');
        $order = input('order');
        $where = array();
        $account = input('account');
        $where[] = ['name', 'like', '%' . $account . '%'];
        $model = $this->where($where)->with(["users", "mediatypes", "medias", "casetypes"])->order("id", "desc")->paginate($pageSize);
        // dump($this->getLastSql());
        return $model;
    }
}