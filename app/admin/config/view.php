<?php
return [
    // 模板后缀
    'view_suffix'   => 'html',
    'tpl_replace_string' => [
        '__STATIC__' => '/static',
    ]
];
    