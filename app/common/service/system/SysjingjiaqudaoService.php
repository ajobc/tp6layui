<?php

namespace app\common\service\system;

use app\admin\model\system\SysjingjiaqudaoModel;
use think\Service;

class SysjingjiaqudaoService extends Service
{
    public static function getAllList()
    {
        $model = new SysjingjiaqudaoModel();
        return $model->with([])->where([])->order('id','asc')->select();
    }
}