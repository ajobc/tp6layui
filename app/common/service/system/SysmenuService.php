<?php

namespace app\common\service\system;

use app\admin\model\system\SysmenuModel;
use app\admin\model\system\SysprojectModel;
use think\Service;

class SysmenuService extends Service
{
    public static function getAllList()
    {
        $sysmenu = SysmenuModel::getAllMenus();
        return $sysmenu;
        
    }
}