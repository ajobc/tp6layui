<?php

namespace app\common\service\system;

use app\admin\model\system\SysmediaModel;
use app\admin\model\system\SysuserModel;
use think\Service;

class SysuserService extends Service
{
    public static function getAllList()
    {
        $model = new SysmediaModel();
        return $model->with(['mediatype'])->where("status", '可用')->order('mediatype_id', 'asc')->select();
    }

    public static function getUsertananListInCase()
    {
        $model = new SysuserModel();
        $res = $model->alias('a')
                     ->with(["roles", "projects", "department"])
                     ->join(['sysproject_user' => 'pc'], 'a.id=pc.user_id')
                     ->where('pc.project_id', '=', session('projectid'))
                     ->where('a.fenlei', '=', "谈案")
                     ->where('a.status', '=', "可用")
                     ->order('id','asc')
                     ->select();
        // var_dump(Db::getLastSql());
        return $res;
    }
}